# data.table pt3 {-}

## Запись занятия {-}

Запись занятия 19 февраля:

<iframe width="560" height="315" src="https://www.youtube.com/embed/Bu7r7sruMJ4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<br>

## rbind() {-}

Функция `rbind()`(от `row bind`) используется для объединение двух или более таблиц по строкам. То есть, в результате получается таблица с таким же количеством колонок, но с увеличенным числом строк - по количеству строк в объединяемых таблицах.

Нередко в объединяемых таблицах отсутствует какая-нибудь колонка или колонки перепутаны. В таких случаях необходимо использовать аргументы `use.names = TRUE` (проверка названий колонок при объединение) и `fill = TRUE` (создание колонки с `NA`-значениями). Обратите внимание, это работает только с `data.table`-объектами.
```{r 4-dt-pt1-18}
library(data.table)

# создаем первую таблицу
df1 <- data.table(tb = 'table_1',
                  col1 = sample(9, 3),
                  col3 = 'only in table1',
                  col2 = sample(letters, 3))

# создаем вторую таблицу
df2 <- data.table(tb = 'table_2',
                  col4 = 'only in table2',
                  col1 = sample(9, 3),
                  col2 = sample(letters, 3))

# объединяем по строкам
rbind(df1, df2, fill = TRUE)
```


## merge() {-}
Одна из самых, наверное, важных операций при работе с таблицами - построчное слияние двух или нескольких таблиц. При использовании функции `merge()` каждому значению в ключевой колонке первой таблицы сопоставляется строка параметров наблюдения другой таблицы, с таким же значением в ключевой колонке, как и в первой таблице. В других языках программирования, в SQL, в частности, аналогичная функция может называться `join`.
Несмотря на сложность формулировки, выглядит это достаточно просто:
```{r}
# создаем датасет 1, в синтасисе data.table
dt1 <- data.table(key_col = c('r1', 'r2', 'r3'),
                  col_num = seq_len(3))

# создаем датасет 2, в синтасисе data.table
dt2 <- data.table(key_col = c('r3', 'r1', 'r2'),
                  col_char = c('c', 'a', 'b'))

# сливаем построчно по значениям в колонке key_col
merge(x = dt1, y = dt2, by = 'key_col')
```

Здесь первая таблица задается аргументом `x`, вторая таблица - аргументом `y`, а колонка (или колонки), по значениям которой происходит слияние таблиц, задается аргументом `by`. Если аргумент `by` не указан, то слияние происходит по тем колонкам, которые имеют одинаковое название в сливаемых таблицах.  Притом, таблицы можно сливать по значениям колонок разными именами, тогда надо отдельно указать, по значениям каких колонок в первой и второй таблице происходит слияние, и для этого вместо общего аргумента `by` используют аргументы `by.x` и `by.y` для первой и второй таблицы соответственно.

В первом приближении операция слияния `merge()` похожа на результат работы функции `cbind()`. Однако, из-за того, что при слиянии происходит сопоставление по значениям ключевых колонок, в результате получается решается проблема слияния колонок, в которых разный порядок строк. Сравните:

```{r}
cbind(dt1, dt2)

merge(x = dt1, y = dt2, by = 'key_col')
```


Второе существенное отличие от `cbind()` - обработка ситуаций, когда в таблицах разное количество наблюдений. Например, в первой таблице данные по первой волне опросов, а во второй - данные по тем, кто из принявших участие в первой волне, принял участие и во второй волне, а так же какие-то новые опрошенные респонденты. Разное количество наблюдений в сливаемых таблицах порождает четыре варианта слияния, все они задаются аргументом `all` с постфикасми.

Создаем данные:
```{r}
# создаем данные первой волны
wave1 <- data.table(id = paste0('id_', seq_len(5)),
                    col1 = sample(10, 5))
wave1

# создаем данные второй волны
wave2 <- data.table(id = paste0('id_', c(1, 3, 5, 6, 7, 8)),
                    col2 = sample(letters, 6))
wave2
```

Варианты направлений слияния (мерджа) таблиц:

- `all` = FALSE. Значение аргумента по умолчанию, в результате слияния будет таблица с наблюдениями, которые есть и в первой, и во второй таблице. То есть, наблюдения из первой таблицы, которым нет сопоставления из второй таблицы, отбрасываются. В примере с волнами это будет таблица только по тем, кто принял участи и в первой, и во второй волнах опросов:
```{r}
# сливаем так, чтобы оставить только тех, кто был в обеих волнах, это зачение по умолчанию
merge(x = wave1, y = wave2, by = 'id', all = FALSE)
```

- `all.x` = TRUE. Всем наблюдениям из первой таблицы сопоставляются значения из второй. Если во второй таблице нет соответствующих наблюдений, то пропуски заполняются `NA`-значениями (в нашем примере в колонке `col2`):
```{r}
# сливаем так, чтобы оставить тех, кто был в первой волне
merge(x = wave1, y = wave2, by = 'id', all.x = TRUE)
```

- `all.y` = TRUE. Обратная ситуация, когда всем наблюдениям из второй таблицы сопоставляются значения из первой, и пропущенные значения заполняются `NA`-значениями (в нашем примере в колонке `co12`):
```{r}
# сливаем так, чтобы оставить тех, кто был во второй волне
merge(x = wave1, y = wave2, by = 'id', all.y = TRUE)
```

- `all` = TRUE. Объединение предыдущих двух вариантов - создается таблица по всему набору уникальных значений из ключевых таблиц, по которым происходит слияние. и если в какой-то из таблиц нет соответствующих наблюдений, то пропуски также заполняются `NA`-значениями:

```{r}
# сливаем так, чтобы оставить тех, кто был в какой-то из обеих волн
merge(x = wave1, y = wave2, by = 'id', all = TRUE)
```


При работе с несколькими таблицами можно столкнуться с ограничением, что базовая функциz `merge()` работает только с парами таблиц. То есть, если вдруг необходимо слить по одному ключу сразу несколько таблиц (например, не две волны опросов, а пять), то придется строить последовательные цепочки попарных слияний.


**Alarm!**
Необходимо помнить, что в ситуациях, когда одному значению ключа в первой таблице соответствует одна строка, а во второй таблице - несколько строк, то в результате слияния таблиц значения из первой таблицы размножатся по количеству строк во второй таблице:
```{r}
# таблица, где на одно значение колонки ключа есть одна строка
dt1 <- data.table(
  key_col = c('a', 'b', 'c'),
  col1 = 1:3
)
dt1

# на одно значение ключа (key_col = b)есть три строки
dt2 <- data.table(
  key_col = c('a', 'b', 'b', 'b', 'c'),
  col2 = rnorm(5)
) 
dt2
```


Сливаем и получаем размножение значений из первой таблицы для ключа `key_col = b`, значение `2` из колонки `col1` теперь встречается три раза:
```{r}
merge(dt1, dt2, by = 'key_col', all.x = TRUE)
```


## Домашнее задание {-}

### Level 1: I'm too young to die {-}

 - внимательно прочитайте материалы занятия и разберитесь с примерами.
 
 - обновите в памяти операции над векторами (создание, простейшие манипуляции, фильтрация по номеру элемента и по значению), а так же операции с таблицами - создание, фильтрация по строкам, операции над колонками, агрегаты.

<br>

### Level 2: Hey, not too rough {-}
- выполните следующие выражения. Посмотрите класс объектов, при необходимости сконвертируйте в data.table:
```{r}
library(data.table)
# таблица привлеченных пользователей и их стоимости
users <- fread('https://gitlab.com/hse_mar/mar211s/raw/main/data/users.csv')
str(users)

# таблица платежей привлеченных пользователей
payments <- fread('https://gitlab.com/hse_mar/mar211s/raw/main/data/payments.csv')
str(payments)
```

- Посчитайте среднюю стоимость (`cost`) привлечения пользователей для каждого рекламного канала (`media_source`). В результате у вас должна получиться такая таблица:

```{r, echo=FALSE}
users[, list(cost_mn = round(mean(cost), 2)), by = media_source]
```


- Посчитайте сколько в среднем платят пользователи (`gross`) каждого канала привлечения пользователей. В результате у вас должна получиться такая таблица:

```{r, echo=FALSE}
merge(payments, users, by = 'user_id', all.x = TRUE)[, list(gross_mn = round(sum(gross), 2)), by = media_source]
```


### Level 3: Hurt me plenty {-}

- Повторите основные задания и добавьте к средней стоимости / платежам еще и общее количество пользователей, и количество пользователей, сделавших платеж. Для подсчета количества пользователей используйте связку функций `length()` + `unique()` или `uniqueN()`

- Добавьте строчку `Total` - все статистики, но без разбивки по источником привлечения пользователей.
В результате у вас должна получиться таблица:

```{r, echo=FALSE}
dt3 <- rbind(users, copy(users)[, media_source := 'Total'], use.names = TRUE)
dt4 <- merge(payments, users, by = 'user_id', all.x = TRUE)
dt4 <- rbind(dt4, copy(dt4)[, media_source := 'Total'], use.names = TRUE)

merge(
  dt3[, list(
    total_users = uniqueN(user_id), 
    cost_mn = round(mean(cost), 2)), by = media_source],
  dt4[, list(
    payers = uniqueN(user_id), 
    gross_mn = round(sum(gross), 2)), by = media_source],
  by = 'media_source',
  all.x = TRUE
)
```

### Level 4: Ultra-violence {-}

NB! задания не связаны друг с другом, просто отработка разных алгоритмов.

 - Отсортируйте датасет по пользователям. Создайте колонку кумулятивных платежей `gross_cum` по каждому пользователю (кумулята в рамках пользователя). Создайте колонку, в которой будет доля этого значения кумуляты от общей суммы. Вам поможет идея, что делать операции над колонками в группах можно не только при создании новой таблицы, но и при создании новых колонок.
 
```{r, echo=FALSE}
payments <- fread('https://gitlab.com/hse_mar/mar211s/raw/main/data/payments.csv')
payments <- payments[order(user_id)]
payments[, gross_cum := cumsum(gross), by = user_id]
payments[, share := round(gross_cum / gross_cum[.N], 2), by = user_id]
payments[1:5]

```

 - Создайте переменную-маркер `gross_quant`, в котором будет маркировка пользователей, в каком квартиле находятся суммарные платежи пользователя. Подумайте, погуглите (напрямую задача вряд ли решается), как сделать это максимально лаконично (достаточно читаемо это можно сделать в два выражения, например).
 
```{r, echo=FALSE}
gross_stat <- payments[, list(gross_total = sum(gross)), by = user_id]
gross_stat[, gross_quant := cut(gross_total, breaks = quantile(gross_total), labels = paste('Q', 1:4, sep = ''), include.lowest = TRUE)]
payments <- merge(payments, gross_stat[, list(user_id, gross_quant)], by = 'user_id', all.x = TRUE)
payments[1:5]
```

<br>

### Level 5: Nightmare {-}
 - Перезагрузите payments. 
 
 - Для каждого платящего пользователя сгенерируйте метрику, на какой день от инсталла пользователь сделал платеж (округляйте до целых). Для генерации используйте `rlnorm()` c meanlog = 1, sdlog = 0.5.  
 
 - Посчитайте сколько в среднем дней проходит между первым и третьим платежами, с разбивкой по каналам привлечения.
 
 - Сделайте все это только с одним мерджем - присоединением канала пользователя. В принципе, четырех выражений вполне хватит. Если воспользоваться некоторыми трюками и пренебречь эстетикой - то и двух.

```{r, echo=FALSE}
payments <- fread('https://gitlab.com/hse_mar/mar211s/raw/main/data/payments.csv')
payments <- merge(payments, users[, list(user_id, media_source)], by = 'user_id', all.x = TRUE)
payments[, purchase_day := round(sort(rlnorm(.N, 1, 0.5))), by = user_id]
payments_stat <- payments[, 
                          list(delta = purchase_day[3] - purchase_day[1]), 
                          by = list(media_source, user_id)]
payments_stat[, list(delta_mn = mean(delta, na.rm = TRUE)), by = media_source]


# payments[, purchase_day := round(sort(rlnorm(.N, 1, 0.5))), by = user_id][
#   , purchase_day[3] - purchase_day[1], by = list(media_source, user_id)][
#     , mean(V1, na.rm = TRUE), by = media_source]
```

```{r, include=FALSE}
payments[, `:=`(purchase_day = NULL, media_source = NULL)]
```

